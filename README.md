## futures-shuttle
[![Build status](https://gitlab.com/imp/futures-shuttle-rs/badges/master/pipeline.svg)](https://gitlab.com/imp/futures-shuttle-rs/commits/master)

Futures-aware shuttle synchronization object

Creates a new shuttle synchronization object for sharing values between two asynchronous tasks.
Each half can be separately owned and sent across tasks.

### Examples

```
extern crate futures;
extern crate futures_shuttle;

use std::thread;

use futures_shuttle::shuttle;
use futures::*;

fn main() {
    let (mut left, mut right) = shuttle(42);

    assert!(left.is_mine());
    assert!(!right.is_mine());
    assert_eq!(left.get(), 42);
    left.set(84);
    left.send();
    assert!(!left.is_mine());
    assert!(right.is_mine());
    assert_eq!(right.get(), 84);
}
```
